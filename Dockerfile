FROM ubuntu:latest

ARG VERSION=2.22.0-1
ENV VERSION=$VERSION

RUN apt-get update \
 && apt-get install -y \
      curl \
      wget \
 && apt-get clean

RUN wget https://lilypond.org/download/binaries/linux-64/lilypond-$VERSION.linux-64.sh -O install.sh \
 && chmod 755 install.sh \
 && ./install.sh --batch \
 && rm install.sh

VOLUME /workspace
WORKDIR /workspace
