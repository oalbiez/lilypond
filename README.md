
# LilyPond container

This is my [LilyPond](http://lilypond.org) container used.

# Usage

use the `/workspace` volume to mount your tex directory and run :

      docker run --rm -it -v $(pwd):/workspace registry.gitlab.com/oalbiez/lilypond make
